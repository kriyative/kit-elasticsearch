(ns kit.elasticsearch.core
  (:require
   [clojure.string :as str]
   [kit.util :as u]
   [kit.assert :as a]
   [kit.http-client :as hc]))

(def default-host (atom nil))

(defn set-default-host! [host]
  (reset! default-host host))

(def ^:dynamic *host* nil)
(defmacro with-host [host & body]
  `(binding [*host* ~host]
     ~@body))

(defn get-host []
  (or *host* @default-host))

(defn request
  ([url]
   (request url nil))
  ([url body]
   (request url body nil))
  ([url body {:as opts :keys [method headers]}]
   (a/assert-not-empty! (get-host)
                        "The elasticsearch host is not bound"
                        (u/dissoc-if-nil?
                         {:url url :body body :opts opts}))
   (let [request (u/dissoc-if-nil?
                  {:method (or method :get)
                   :headers (merge {:content-type "application/json"}
                                   headers)
                   :url (format "%s/%s" (get-host) url)
                   :body body})
         {:keys [status response]} (hc/request request)]
     (a/assert! (= :ok status)
                "ElasticSearch request error"
                {:request request :response response})
     (:body response))))

(defn query [index body]
  (request (format "%s/_search" index) body))

(defn as-query-string [query-spec]
  (cond
    (map? query-spec)
    (str "("
         (->> query-spec
              (map
               (fn [[k v]]
                 (str (name k) ":" (as-query-string v))))
              (str/join " AND "))
         ")")

    (set? query-spec)
    (str "("
         (->> query-spec
              (map as-query-string)
              (str/join " OR "))
         ")")

    :else
    (pr-str query-spec)))
